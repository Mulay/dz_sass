'use strict';

const gulp = require('gulp');
const gulpSpritesmith = require('gulp.spritesmith');

module.exports = function(options) {

    return function () {
        var spriteData = gulp.src(options.src)
            .pipe(gulpSpritesmith({
                    imgName: 'sprite.png',
                    cssName: 'sprite.scss',
                    cssFormat: 'scss',
                    algorithm: 'binary-tree',
                    cssTemplate: 'scss.template.mustache',
                    imgPath: './img/sprite.png',
                    cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));
        spriteData.css.pipe(gulp.dest('src/styles/tmp'));
        spriteData.img.pipe(gulp.dest('src/assets/img'));
    }
};